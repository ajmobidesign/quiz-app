import React from 'react';
import PropTypes from 'prop-types';
import CleanString from '../app-components/clean-string';


const ResultBox = (props)=>{

	const { myAnswerIsCorrect, question, correctAnswer, myAnswer } = props;
	let checkMark = '&#10003;'
	
	return (
			<div className="results-answer" >
				<div className="answer-container">
					<div className="answer-section">
						<span className="answer-question">
							<CleanString str={question} />
						</span>
						< br/> 
						<span className="answer-result"> 
							{
								(myAnswerIsCorrect)? <span className="correct-answer"> 

									<CleanString str={checkMark} />

								</span> 

								: <span className="wrong-answer"> X </span>
							}
						</span>
					</div>
					<div className="answer-section">
						<div>
							Your Answer : { myAnswer  }
							<br />
							Correct Answer : { correctAnswer }
						</div>
					</div>
				</div>		
			</div>
		)
	}

ResultBox.propTypes = {
	myAnswerIsCorrect : PropTypes.bool.isRequired,
	myAnswer : PropTypes.string.isRequired,
	correctAnswer : PropTypes.string.isRequired,
	question : PropTypes.string.isRequired
};

export default ResultBox;
