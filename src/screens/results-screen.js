import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ResultsBox from '../results-components/results-box';



class ResultsScreen extends Component{

	static propTypes = {
		restart : PropTypes.func.isRequired
	}


	constructor(props){
	    super(props)
	    this.restartGame = this.restartGame.bind(this);
	    this.renderResults = this.renderResults.bind(this);
	}


	restartGame(){
		const { restart } = this.props;
		restart('start');
	}


	renderResults(){
		const { scoreArray, questions } = this.props;

		return scoreArray.map((d, i)=>{

				const { myAnswer, correctAnswer, qIdx, score } = d;

				let myAnswerIsCorrect = (true == score);
				let question = questions[qIdx].question;

				let resultsProps = {
					myAnswerIsCorrect,
					myAnswer,
					question,
					correctAnswer
				};
				
			return <ResultsBox { ...resultsProps} key={i} />
		});
	}

	render(){
		const { scoreArray } = this.props;
		
		let totalScore = scoreArray.map((d)=> d.score).reduce((a, b)=> a+b);

		return (

			<div className="row results-container">

				<div className="col-12 score-container">
					<p> You scored { totalScore } /10 </p>
				</div>
				<div className="col-12 results-answers-container">
					<div className="results-answer">
						{ this.renderResults()}
					</div>	
				</div>	
				<div className="col-12">
					<button type="button" className="btn btn-info" onClick={this.restartGame}> Play Again! </button>
				</div>
			</div>
		)
	}
}

export default ResultsScreen;