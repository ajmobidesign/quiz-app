import React, { Component } from 'react';
import PropTypes from 'prop-types';


class StartScreen extends Component{

	static propTypes = {
		start : PropTypes.func.isRequired
	}	

	constructor(props){
	    super(props)
	    this.start = this.start.bind(this);
	}

  	start(){
  		const { start } = this.props;
  		start('loading');
  	}


	render(){
		return (
			<div className="row start-container">
				<div className="col-12 start-header">
					<h4 className="display-4"> Welcome To Trivia Challenge </h4>
				</div>
				<div className="col-12">
					<p className="lead">You will be presented with 10 True or False questions.
						<br />
						<br />
						Can you score 100%?
					</p>

				</div>
				<div className="col-12">
					<button type="button" className="btn btn-danger btn-lg" onClick={this.start} > Start Now </button>
				</div>
			</div>
		)
	}
}

export default StartScreen;