import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TrueFalseButton from '../quiz-components/true-false-button';
import QuestionCard from '../quiz-components/question-card';
import { CSSTransitionGroup } from 'react-transition-group';



class QuizScreen extends Component{

	static propTypes = {
		questions : PropTypes.array,
		setScoreArray : PropTypes.func,
		scoreArray : PropTypes.array,
		currentQIdx : PropTypes.number,
		setCurrentQuestionIdx : PropTypes.func
	}

	constructor(props){
	    super(props)
	    this.setAnswer = this.setAnswer.bind(this);
	}


	setAnswer(answerOb){
		const { scoreArray, setScoreArray, setCurrentQuestionIdx} = this.props;
		let newArr = [].concat(scoreArray);
		newArr.push(answerOb);
		setScoreArray(newArr);
		setCurrentQuestionIdx();
	}


	render(){

		const {  currentQIdx, questions } = this.props;

		let qIdx = currentQIdx;

		const { question, correct_answer, category } = questions[qIdx];

		return (

			<div className="row quiz-container">
				<div className="col-12 quiz-header">
					<p> { qIdx +1 }  of 10 </p>
				</div>
				<div className="col-12 quiz-content">
					<div className="question-box">	

						<CSSTransitionGroup
				          transitionName="fade"
				          transitionEnterTimeout={200}
				          transitionLeaveTimeout={200}>

							<QuestionCard question={question} category={category} idx={qIdx} key={qIdx}/>

						</CSSTransitionGroup>						
					</div>
				</div>
				<div className="col-12 quiz-buttons">
					<TrueFalseButton value="True" qIdx={qIdx} setAnswer={this.setAnswer} correctAnswer={correct_answer} />

					<TrueFalseButton value="False" qIdx={qIdx} setAnswer={this.setAnswer} correctAnswer={correct_answer} />
				</div>
			</div>
		)
	}
}

export default QuizScreen;