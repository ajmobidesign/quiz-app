import React from 'react';
import PropTypes from 'prop-types';
import ResultsScreen from './results-screen';
import QuizScreen from './quiz-screen';
import StartScreen from './start-screen';
import LoadingScreen from './loading-screen';


const AppScreens = (props)=>{

	 const { 
	 		qIdx, 
	 		scoreArray, 
	 		status, 
	 		questions, 
	 		setScoreArray, 
	 		setCurrentQuestionIdx, 
	 		setQuizStatus } = props;

	 switch(status){
	 	  	case 'start':
	 	 		return <StartScreen start={setQuizStatus} />;

           	case 'loading': 
           		return <LoadingScreen status={status} />;

           	case 'error': 
           		return <LoadingScreen status={status} />;	

            case 'active':
            	return <QuizScreen 
	                      setCurrentQuestionIdx={setCurrentQuestionIdx} 
	                      currentQIdx={qIdx} 
	                      questions={questions}  
	                      scoreArray={scoreArray} 
	                      setScoreArray={setScoreArray} />;          
            case 'end':
            	return <ResultsScreen  
                        restart={setQuizStatus} 
                        scoreArray={scoreArray} 
                        questions={questions} />;
            default: 
				return <div />;	            
	 }
}


AppScreens.propTypes = {
	qIdx : PropTypes.number.isRequired,
	scoreArray : PropTypes.array.isRequired,
	status : PropTypes.string.isRequired,
	questions : PropTypes.array.isRequired,
	setScoreArray : PropTypes.func.isRequired,
	setCurrentQuestionIdx : PropTypes.func.isRequired,
	setQuizStatus : PropTypes.func.isRequired
};

export default AppScreens;
