import React, { Component } from 'react';
import PropTypes from 'prop-types';


class LoadingScreen extends Component{

	static propTypes = {
		status : PropTypes.string.isRequired
	}

	render(){
		const { status } = this.props;

		return(
			<div className="row loading-container">
				<div className="col-12 loading-content">
					{(status === 'loading')? <h5 className="display-4"> Quiz is loading... </h5> : null}
					{(status === 'error')? <h5 className=""> There was an error please reload the app</h5> : null}
				</div>
			</div>
		)
	}
}

export default LoadingScreen;