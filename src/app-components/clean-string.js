import React from 'react';
import PropTypes from 'prop-types';

const CleanString=({str})=>{
	return <span dangerouslySetInnerHTML={{__html: str}} />;
}

CleanString.propTypes = {
	str : PropTypes.string.isRequired
};

export default CleanString;