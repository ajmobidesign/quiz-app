import React from 'react';
import PropTypes from 'prop-types';
import CleanString from '../app-components/clean-string';


const QuestionCard = ({ category, question, idx })=>{

	return(
		<div className="card text-center question-card" id={'qcard_'+idx}>
			<div className="card-body">
				<h5 >{ category }</h5>
				<p className="card-text"> <CleanString str={question} /> </p>
			</div>	
		</div>
	)
};

QuestionCard.propTypes = {
	category : PropTypes.string.isRequired,
	question : PropTypes.string.isRequired,
	idx : PropTypes.number.isRequired
};

export default QuestionCard;