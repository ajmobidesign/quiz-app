import React, { Component } from 'react';
import PropTypes from 'prop-types';



class TrueFalseButton extends Component{

	static propTypes = {
		value : PropTypes.string.isRequired, 
		setAnswer : PropTypes.func.isRequired,
		correctAnswer : PropTypes.string.isRequired,
		qIdx : PropTypes.number.isRequired
	}

	constructor(props){
	    super(props);
	    this.click = this.click.bind(this);
	}

	click(){
		const { value, setAnswer, correctAnswer,  qIdx } = this.props;

		let myAnswerIsCorrect = (value === correctAnswer);
		let answerOb = { 
			myAnswer : value,
			correctAnswer,
			qIdx,
			score :(myAnswerIsCorrect)? 1 : 0
		};

		setAnswer(answerOb);
	}    


	render(){

		const { value } = this.props;
		return(
			<button type="button" className="btn btn-primary" onClick={this.click}> { value } </button>
		)
	}
}

export default TrueFalseButton;
