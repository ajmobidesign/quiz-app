import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { quizActions } from './core/quiz';
import AppScreens from './screens/app-screens';


class App extends Component {

  constructor(props){

    super(props);
    this.setQuizStatus = this.setQuizStatus.bind(this);
    this.setCurrentQuestionIdx = this.setCurrentQuestionIdx.bind(this);
    this.setScoreArray = this.setScoreArray.bind(this);
  }

  setQuizStatus(status){

    const { setQuizStatus, getQuizQuestions, restartQuiz } = this.props;    

    if(status==='loading'){
        getQuizQuestions();
        restartQuiz();
    }
    setQuizStatus(status);
  }


  setCurrentQuestionIdx(){

    const { loadNextQuestion, setQuizStatus, qIdx } = this.props;
    let nextIdx = qIdx+1;

    if(nextIdx===10){ 
      setQuizStatus('end');
    }
    else{
      loadNextQuestion(nextIdx);
    }
  }

  setScoreArray(arr){
    const { updateScore } = this.props;  
    updateScore(arr);
  }



  render() {
    const { qIdx, scoreArray, status, questions } = this.props;

    let appProps ={
                  qIdx, 
                  scoreArray, 
                  status, 
                  questions, 
                  setScoreArray : this.setScoreArray,
                  setCurrentQuestionIdx : this.setCurrentQuestionIdx,
                  setQuizStatus : this.setQuizStatus
                }

    return (
        <div className="main-container container">
             <AppScreens  { ...appProps} />        
        </div>
    );
  }
}

const mapStateToProps = (state)=>{
  return  state.quiz;
};

const mapDispatchToProps = (dispatch)=>{
  return bindActionCreators(Object.assign({}, quizActions), dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
