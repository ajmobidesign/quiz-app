import { quizReducer } from './reducer';
import {
	SET_QUIZ_QUESTIONS,
	SET_QUIZ_STATUS,
	RESTART_QUIZ, 
	UPDATE_SCORE,
	LOAD_NEXT_QUESTION
}	from './action-types';

describe('quiz reducer', () => {
	it('should return the initial state', () => {
	    expect(quizReducer(undefined, {})).toEqual(
	      	{
				status : 'start',
				qIdx : 0,
				scoreArray : [], 
				questions : []
			}
	    )
  	})


  	it('should reset to initial state', () => {
	    expect(quizReducer(
	    		undefined, 
		    	{
		    		type: RESTART_QUIZ
		    	}
	    )).toEqual(
	      	{
				status : 'start',
				qIdx : 0,
				scoreArray : [], 
				questions : []
			}
	    )
  	})


  	it('should set quiz questions', () => {
	    expect(quizReducer(
	    		{
					status : 'start',
					qIdx : 0,
					scoreArray : [], 
					questions : []
				}, 
		    	{
		    		type: SET_QUIZ_QUESTIONS,
		    		payload: [{q1: 1, q2: 2, q3: 3}]
		    	}
	    )).toEqual(
	      	{
				status : 'start',
				qIdx : 0,
				scoreArray : [], 
				questions : [{q1: 1, q2: 2, q3: 3}]
			}
	    )
  	})

  	it('should change quiz status', () => {
	    expect(quizReducer(
	    		{
					status : 'start',
					qIdx : 0,
					scoreArray : [], 
					questions : []
				}, 
		    	{
		    		type: SET_QUIZ_STATUS,
		    		payload: 'quiz'
		    	}
	    )).toEqual(
	      	{
				status : 'quiz',
				qIdx : 0,
				scoreArray : [], 
				questions : []
			}
	    )
  	})

  	it('should increment index', () => {
	    expect(quizReducer(
	    		{
					status : 'quiz',
					qIdx : 0,
					scoreArray : [], 
					questions : []
				}, 
		    	{
		    		type: LOAD_NEXT_QUESTION,
		    		payload: 1
		    	}
	    )).toEqual(
	      	{
				status : 'quiz',
				qIdx : 1,
				scoreArray : [], 
				questions : []
			}
	    )
  	})

  	it('should update score array', () => {
	    expect(quizReducer(
	    		{
					status : 'quiz',
					qIdx : 0,
					scoreArray : [{ x : 0, y: 0}], 
					questions : []
				}, 
		    	{
		    		type: UPDATE_SCORE,
		    		payload: [{ x : 0, y: 0},{ x : 1, y: 0}]
		    	}
	    )).toEqual(
	      	{
				status : 'quiz',
				qIdx : 0,
				scoreArray : [{ x : 0, y: 0},{ x : 1, y: 0}], 
				questions : []
			}
	    )
  	})
})