import {
	SET_QUIZ_QUESTIONS,
	SET_QUIZ_STATUS,
	RESTART_QUIZ, 
	UPDATE_SCORE,
	LOAD_NEXT_QUESTION
}	from './action-types';

import axios from 'axios';

let dataURL = 'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean';


export const getQuizQuestions = ()=>{
	return (dispatch)=>{
	  return axios.get(dataURL)
		.then((res)=>{
			setTimeout(()=>{
				if(res.data && res.data.results && res.data.results.length>0){
					dispatch(setQuizQuestions(res.data));
					dispatch(setQuizStatus('active'));
				}
				else{
					dispatch(setQuizStatus('error'));	
				}	
			}, 500)
		})
		.catch(error => {
			dispatch(setQuizStatus('error'));
		    throw(error);
		});
	};
};


export const setQuizQuestions = (response)=>{
	const { results } = response;
	return{
		type: SET_QUIZ_QUESTIONS,
		payload: results
	};
};

export const setQuizStatus = (status)=>{
	return {
		type: SET_QUIZ_STATUS,
		payload : status
	};
};

export const updateScore = (scoreArray)=>{
	return {
		type : UPDATE_SCORE,
		payload : scoreArray
	};
};


export const loadNextQuestion=(idx)=>{
	return {
		type : LOAD_NEXT_QUESTION,
		payload : idx
	};
};

export const restartQuiz = ()=>{
	return {
		type : RESTART_QUIZ
	};
};


