import {
	restartQuiz,
	loadNextQuestion,
	updateScore,
	setQuizStatus,
	setQuizQuestions

}  from './actions';
import {
	SET_QUIZ_QUESTIONS,
	SET_QUIZ_STATUS,
	RESTART_QUIZ, 
	UPDATE_SCORE,
	LOAD_NEXT_QUESTION
}	from './action-types';



describe('quiz reducer', () => {

	it('should create an action to update quiz status', () => {
	    expect(setQuizQuestions(
	    		{ results: [{x: 1}, { y: 2}, { z: 3}] }
	    	)).toEqual(
	      	{
				type: SET_QUIZ_QUESTIONS, 
				payload : [{x: 1}, { y: 2}, { z: 3}]
			}
	    )
  	})

	it('should create an action to update quiz status', () => {
	    expect(setQuizStatus(
	    		'quiz'
	    	)).toEqual(
	      	{
				type: SET_QUIZ_STATUS, 
				payload : 'quiz'
			}
	    )
  	})


	it('should create an action to update score array', () => {
	    expect(updateScore(
	    		[{x: 1, y : 1}]
	    	)).toEqual(
	      	{
				type: UPDATE_SCORE, 
				payload : [{x: 1, y : 1}]
			}
	    )
  	})


	it('should create an action to increment index', () => {
	    expect(loadNextQuestion(
	    		1
	    	)).toEqual(
	      	{
				type: LOAD_NEXT_QUESTION, 
				payload : 1
			}
	    )
  	})

	it('should create an action to restart state', () => {
	    expect(restartQuiz()).toEqual(
	      	{
				type: RESTART_QUIZ
			}
	    )
  	})
})  	

