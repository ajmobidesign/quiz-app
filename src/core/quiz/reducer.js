
import {
	SET_QUIZ_QUESTIONS,
	SET_QUIZ_STATUS,
	RESTART_QUIZ, 
	UPDATE_SCORE,
	LOAD_NEXT_QUESTION
}	from './action-types';


const quizQuestions = { questions : [] };

const appStates = {
	status : 'start',
	qIdx : 0,
	scoreArray : []
};

const newState = { ...appStates, ...quizQuestions };


export const quizReducer = (state = newState, action )=>{

		switch(action.type){

			case SET_QUIZ_QUESTIONS: 
				return Object.assign({}, state, { questions : action.payload})

			case SET_QUIZ_STATUS:
				return Object.assign({}, state, {status : action.payload});

			case LOAD_NEXT_QUESTION:
				return Object.assign({}, state, {qIdx : action.payload});

			case UPDATE_SCORE:
				return Object.assign({}, state, {scoreArray : action.payload});	

			case RESTART_QUIZ:
				return Object.assign({}, state, appStates);	

			default: 
				return state;
		}
}		