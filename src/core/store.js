import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';


const enhancers = [];
const middleware = [
  thunkMiddleware
]; 

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);


const logger = createLogger({
  level: 'info',
  collapsed: true,
});


if (process.env.NODE_ENV === 'development') {
	  middleware.push(logger);
	}; 


if(module.hot) {
    module.hot.accept(function _() {
      composedEnhancers.replaceReducer(rootReducer);
    });
  };


const store = createStore(
    rootReducer,
    composedEnhancers
);

export default store;